terraform {
  backend "s3" {
    bucket  = "awsbackendforterraform"
    key     = "terraform.tfstate"
    region  = "us-east-1"
    arn     = "arn:aws:s3:::awsbackendforterraform/terraform.tfstate"
  }
}
